/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week4_1;

import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    /**
     * Protein name.
     */
    private final String name;

    /**
     * Protein accession.
     */
    private final String accession;

    /**
     * Protein sequence.
     */
    private final String aminoAcidSequence;

    /**
     * Protein annotation.
     */
    private GOannotation goAnnotation;

    /**
     * Map of amino acids and their weights.
     */
    private static HashMap<String, Double> aaWeights = new HashMap();

    /**
     * constructs without GO annotation.
     * @param proteinName name of the protein
     * @param proteinAccession accession of the protein
     * @param proteinAminoAcidSequence amino acid sequence
     */
    public Protein(final String proteinName, final String proteinAccession,
            final String proteinAminoAcidSequence) {
        this.name = proteinName;
        this.accession = proteinAccession;
        this.aminoAcidSequence = proteinAminoAcidSequence;
    }

    /**
     * constructs with main properties.
     * @param proteinName name of the protein
     * @param proteinAccession accession of the protein
     * @param proteinAminoAcidSequence amino acid sequence
     * @param proteinGOAnnotation GO annotation
     */
    public Protein(final String proteinName, final String proteinAccession,
            final String proteinAminoAcidSequence,
            final GOannotation proteinGOAnnotation) {
        this.name = proteinName;
        this.accession = proteinAccession;
        this.aminoAcidSequence = proteinAminoAcidSequence;
        this.goAnnotation = proteinGOAnnotation;
    }

    @Override
    public final int compareTo(final Protein o) {
        return 0;
    }

    /**
     * Compares protein names alphabetically.
     * @param o1 first protein
     * @param o2 second protein
     * @return comparison of two protein names.
     */
    private static class CompareProteinName implements Comparator<Protein> {
        @Override
        public int compare(final Protein o1, final Protein o2) {
            return o1.name.toLowerCase().compareTo(o2.name.toLowerCase());
        }
    }

    /**
     * Compares protein names.
     * @param pro1 first protein
     * @param pro2 second protein
     * @return comparison of the protein names.
     */
    static class CompareAccessionNumber implements Comparator<Protein> {
        @Override
        public int compare(final Protein o1, final Protein o2) {
            return o1.accession.toLowerCase().compareTo(
                    o2.accession.toLowerCase());
        }
    }

        /**
     * Compares protein names.
     * @param pro1 first protein
     * @param pro2 second protein
     * @return comparison of the protein names.
     */
    static class CompareProteinWeight implements Comparator<Protein> {
        @Override
        public int compare(final Protein o1, final Protein o2) {
            return o1.aminoAcidSequence.toUpperCase().compareTo(
                    o2.aminoAcidSequence.toUpperCase());
        }
    }

    /**
     * Compares protein names.
     * @param pro1 first protein
     * @param pro2 second protein
     * @return comparison of the protein names.
     */
    static class CompareGOAnnotation implements Comparator<Protein> {
        @Override
        public int compare(final Protein o1, final Protein o2) {
            int compareProcess = o1.goAnnotation.getBiologicalProcess()
                    .compareTo(o2.goAnnotation.getBiologicalProcess());
            if (compareProcess != 0) {
                return compareProcess;
            } else {
                int compareComponent = o1.goAnnotation.getCellularComponent()
                        .compareTo(o2.goAnnotation.getCellularComponent());
                if (compareComponent != 0) {
                    return compareComponent;
                } else {
                    return o1.goAnnotation.getMolecularFunction().compareTo(
                    o2.goAnnotation.getMolecularFunction());
                }
            }
        }
    }

    /**
     * Provides a range of possible sorters based on the type that is requested.
     * @param type sorting type
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(final SortingType type) {
        if (type == SortingType.PROTEIN_NAME) {
            return new CompareProteinName();
        } else if (type == SortingType.ACCESSION_NUMBER) {
            return new CompareAccessionNumber();
        } else if (type == SortingType.GO_ANNOTATION) {
            return new CompareGOAnnotation();
        } else if (type == SortingType.PROTEIN_WEIGHT) {
            return new CompareProteinWeight();
        } else {
            throw new IllegalArgumentException("Type " + type + " is invalid."
            + " Please adjust the value.");
        }
    }

    /**
     *
     * @return name the name
     */
    public final String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public final String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public final String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation
     */
    public final GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public final String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession
                + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }

}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week4_1;

/**
 * Models the GO annotation of a protein.
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class GOannotation {
    /**
     * ID of the GO annotation.
     */
    private final int id;

    /**
     * Cellular components of the protein.
     */
    private final String cellularComponent;

    /**
     * Molecular function of the protein.
     */
    private final String molecularFunction;

    /**
     * Biological process of the protein.
     */
    private final String biologicalProcess;

    /**
     * constructs with all three GO components.
     * @param goID the GO identifier.
     * @param goCellularComponent cellular components of the protein
     * @param goMolecularFunction molecular function of the protein
     * @param goBiologicalProcess biological process of the protein
     */
    public GOannotation(final int goID, final String goCellularComponent,
            final String goMolecularFunction,
            final String goBiologicalProcess) {
        this.id = goID;
        this.cellularComponent = goCellularComponent;
        this.molecularFunction = goMolecularFunction;
        this.biologicalProcess = goBiologicalProcess;
    }

    /**
     * Gets the ID of the GO annotation.
     * @return ID the GO ID
     */
    public final int getID() {
        return id;
    }

    /**
     * Gets the cellular component of the protein.
     * @return cellularComponent the cellular component
     */
    public final String getCellularComponent() {
        return cellularComponent;
    }

    /**
     * Gets the molecular function of the protein.
     * @return molecularFunction the molecular function
     */
    public final String getMolecularFunction() {
        return molecularFunction;
    }

    /**
     * Gets the biological process of the protein.
     * @return biologicalProcess the biological process
     */
    public final String getBiologicalProcess() {
        return biologicalProcess;
    }
}

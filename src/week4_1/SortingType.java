/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week4_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 */
public enum SortingType {
    /**
     * Alphabetical sort on protein name, ascending.
     */
    PROTEIN_NAME("simple namesort"),

    /**
     * Alphabetical sort, case insensitive, ascending.
     */
    ACCESSION_NUMBER("accession number sort"),

    /**
     * Multi sort on: biological process, cellular component and molecular function.
     */
    GO_ANNOTATION("GO annotation"),

    /**
     * Sorts on molecular weight.
     */
    PROTEIN_WEIGHT("protein molecular weight");

    /**
     * Type of the sort.
     */
    private final String type;

    /**
     * Defines sorting type.
     * @param proteinType sorting type
     */
    private SortingType(final String proteinType) {
        this.type = proteinType;
    }

    /**
     * To string function for type.
     * @return Sorting type as string
     */
    @Override
    public String toString() {
        return "SortingType{" + "type=" + type + '}';
    }
}

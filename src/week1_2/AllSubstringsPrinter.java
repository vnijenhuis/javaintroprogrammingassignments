/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package week1_2;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     * @param args the args
     * @author vnijenhuis
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        //should print left truncated, left aligned
        asp.printAllSubstrings("GATCG", true, true);
        //should print right truncated, left aligned
        asp.printAllSubstrings("GATCG", false, true);
        //should print left truncated, left aligned
        asp.printAllSubstrings("GATCG", true, false);
        //should print right truncated, left aligned
        asp.printAllSubstrings("GATCG", false, false);
        //should print left truncated, left aligned
        asp.printAllSubstrings("AGCGCT", true, false);
        //should print right truncated, left aligned
        asp.printAllSubstrings("AGCGCT", false, false);
    }

    /**
     * will print all possible substrings according to arguments.
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be
     * truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be
     * printed left-aligned (or right-aligned)
     * @author vnijenhuis
     */
    public final void printAllSubstrings(
            final String stringToSubstring,
            final boolean leftTruncated,
            final boolean leftAligned) {
        int length = stringToSubstring.length();
        if (leftTruncated && leftAligned) {
            subString(stringToSubstring, length);
        }
        if (!leftTruncated && leftAligned) {
            truncate(stringToSubstring, length);
        }
        if (leftTruncated && !leftAligned) {
            alignment(stringToSubstring, length);
        }
        if (!leftTruncated && !leftAligned) {
            truncateAlignment(stringToSubstring, length);
        }
    }

    /**
     * Truncates and aligns a string to the left.
     * @param stringToSubstring contains the given string.
     * @param length length of string.
     * Truncate is true, Alignment is true
     */
    public final void subString(final String stringToSubstring,
            final Integer length) {
        for (int i = 0; i < length; i++) {
            String substring = stringToSubstring.substring(i);
            System.out.println(substring);
        }
    }

    /**
     * Truncates the string from the right and aligns it from the left.
     * @param stringToSubstring contains the given string.
     * @param length length of string.
     * Truncate is false, Alignment is true
     */
    public final void truncate(final String stringToSubstring,
            final Integer length) {
        for (int i = length; i > 0; i--) {
            String substring = stringToSubstring.substring(0, i);
            System.out.println(substring);
        }
    }

    /**
     * Aligns a string to the right and truncates it from the left.
     * @param stringToSubstring contains the given string.
     * @param length length of string.
     * Truncate is true, Alignment is false
     */
    public final void alignment(final String stringToSubstring,
            final Integer length) {
        for (int i = 0; i < length; i++) {
            String substring = stringToSubstring.substring(i);
            System.out.format("%" + length + "s", substring);
            System.out.println();
        }
    }

    /**
     * Truncates and aligns the string to the right.
     * @param stringToSubstring contains the given string.
     * @param length length of string.
     * Truncate is false, Alignment is false
     */
    public final void truncateAlignment(final String stringToSubstring,
            final Integer length) {
        for (int i = length; i > 0; i--) {
            String substring = stringToSubstring.substring(0, i);
            System.out.format("%" + length + "s", substring);
            System.out.println();
        }
    }
}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {

    /**
     * An ounce is 28 grams.
     */
    private final int ounce = 28;

    /**
     * A pound is 454 grams.
     */
    private final int pound = 454;

    /**
     * main to be used for testing purposes.
     * @param args argument.
     */
    public static void main(final String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        wus.convertFromGrams(1000);
    }

    /**
     * will return the number of Pounds,
     * Ounces and Grams represented by this quantity of grams,
     * encapsulated in a BritishWeightUnits object.
     * @param grams amount of grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is
     */
    public final BritishWeightUnits convertFromGrams(final int grams) {
        if (grams <= 0) {
            throw new IllegalArgumentException("Amount of grams entered is "
                    + "invalid. Try a positive number instead of " + grams);
        }
        int pounds = grams / pound;
        int ounces = grams % pound / ounce;
        int leftoverGrams = grams % pound % ounce;
        return new BritishWeightUnits(pounds, ounces, leftoverGrams);
    }

}

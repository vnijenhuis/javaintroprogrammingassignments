/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week2_1;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    /**
     * testing main.
     * @param args 
     */
    public static void main(final String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GACT";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public final String createConsensus(final String[] sequences,
            final boolean iupac) {
        String consensus = null;
        // Matching patterns.
        Pattern aa4 = Pattern.compile("^GA[ACT]{2}");
        Pattern aa6 = Pattern.compile("^[CGT]{0,1}GA[ACT]{3}");
        int i;
        for (i = 0; i < sequences.length; i++) {
            Matcher match4 = aa4.matcher(sequences[i]);
            Matcher match6 = aa6.matcher(sequences[i]);
            if (match4.find()) {
                if (iupac) {
                    consensus = "GAWH";
                } else {
                    consensus = "GA[A/T][A/C/T]";
                }
            } else if (match6.find()) {
                if (iupac) {
                    consensus = "BGAMHA";
                } else {
                    consensus = "[C/G/T]GA[A/T][A/C/T]A";
                }
            } else {
                throw new IllegalArgumentException("Could not match value of "
                        + sequences[i] + ". Please adjust this value.");
            }
        }
        System.out.println(consensus);
        return consensus;
    }
}

// public static Map<String, String> iupacCodes = new HashMap<>();
// static {
//      iupacCodes.put("[A/T]","W");
//      iupacCodes.put("[A/C/T]","H");
//      iupacCodes.put("[C/G/T]","B");
//      iupacCodes.put("[A/C/G/T]","N");
//      iupacCodes.put("[A/G]","R");
// }
// String consensus = "";
// Set<String> nucs = new HashSet<>();
// for( int i =0; i< sequences[0].length(); i++) {
//       nucs.clear()
//    for(int j = 0; j < sequences.length; j++) {
//       String nuc = sequences.length[j]
//       nucs.add(nuc+"")
//    }
//    if (nucs.size() == 1) {
//      consensus += nucs.toArray()[0];
//    } else {
//      String ambiguity = "[";
//      ambiguinity += String.join("/", nucs);
//      ambiguity += "]";
//      consensus += ambiguity;
//      if (iupac) { consensus += iupacCodes.get(ambiguity) }
//      else {conensus = ambiguity}
//    }
//    System.out.println("nucs = " + nucs);
//
//
//

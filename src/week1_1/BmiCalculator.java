/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week1_1;

import java.util.Scanner;

/**
 *
 * @author michiel
 */
public class BmiCalculator {
    /**
     * @param keyboard scans the keyboard input.
     */
    private final Scanner keyboard = new Scanner(System.in);

    /**
     * The string messages to accompany BMI categories.
     */
    public static final String[] MESSAGES = new String[]{
        "Ondergewicht",
        "Gezond gewicht",
        "Overgewicht",
        "Obesitas",
        "Morbide Obesitas"
    };

    /**
     * starting point of your application.
     *
     * @param args argument.
     */
    public static void main(final String[] args) {
        //implement top-level logic such as fetching user input and
        // creating a feedback message to user
        BmiCalculator bc = new BmiCalculator();
        double userHeigth = bc.getUserHeight();
        double userWeight = bc.getUserWeight();
        double userBMI = bc.calculateBMI(userWeight, userHeigth);
        String label = bc.getMessage(userBMI);
        System.out.println("Your BMI: " + userBMI);

        //generate output to user

    }

    /**
     * fetches the height of the user.
     * @return height the height in meters
     */
    public final double getUserHeight() {
        System.out.print("Please give your height, in meters (e.g. 1.84): ");
        String input = keyboard.nextLine();
        double height = 0;
        try {
            height = Double.parseDouble(input);
        } catch (NumberFormatException ex) {
            System.out.println("This is no number! aborting...");
            System.exit(0);
        }
        return height;
    }

    /**
     * fetches the height of the user.
     * @return height the height in meters
     */
    public final double getUserWeight() {
        System.out.println("Please give you weight, in kilos (e.g. 80.0): ");
        String input = keyboard.nextLine();
        double weight = 0;
        try {
            weight = Double.parseDouble(input);
        } catch (NumberFormatException ex) {
            System.out.println("This is no number! aborting process.");
            System.exit(0);
        }
        return weight;
    }

    /**
     * Returns the BMI given a weight in kilograms and a height in meters.
     *
     * @param weight the weight in kilograms
     * @param height the length in kilograms
     * @return bmi the body mass index
     * @throws IllegalArgumentException when illegal (zero or below) values are
     * provided for weight and/or length
     */
    public final double calculateBMI(final double weight, final double height) {
        if (weight <= 0 || height <= 0) {
            throw new IllegalArgumentException("Error: both weight and "
                    + "height should be above 0. Given: weight="
                    + weight + ", height=" + height);
        }
        //Gewicht in kilogram / (Lengte in meter * Lengte in meter)
        double calculatedBMI = (weight / (height * height));
        return calculatedBMI;
    }

    /**
     * Will return an appropriate String representation belonging to a given
     * BMI.
     *
     * BMI index Categorie &lt;18.5 Ondergewicht 18.5 - 25.0 Gezond gewicht 25.0
     * - 30.0 Overgewicht 30.0 - 40.0 Obesitas &gt;40 Morbide Obesitas
     *
     * @param bmi the BMI
     * @return message
     * @throws IllegalArgumentException when illegal (zero or below) BMI value
     * is provided
     */
    public final String getMessage(final double bmi) {
        String message = "";
        if (bmi <= 0) {
            throw new IllegalArgumentException("BMI is invalid: " + bmi);
        }
        // Tests the BMI values and returns the appropriate description.
        if (bmi < 18.5) {
            message = "Ondergewicht";
        } else if (bmi >= 18.5 && bmi < 25.0) {
            message = "Gezond gewicht";
        } else if (bmi >= 25.0 && bmi < 30.0) {
            message = "Overgewicht";
        } else if (bmi >= 30.0 && bmi < 40.0) {
            message = "Obesitas";
        } else if (bmi >= 40.0) {
            message = "Morbide Obesitas";
        }
        return message;
    }
}

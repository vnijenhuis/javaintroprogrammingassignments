/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice1_genbank_reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reads a gene bank file.
 * @author vnijenhuis
 */
public class SingleGbkFileReader {
    /**
     * Reads a .gb or .gbk file.
     * @param gbkFile gene bank file
     * @return gbk data as string.
     */
    public final String gbkFileReader(final File gbkFile) {
        String gbkData = null;
        try {
            FileReader gbkReader = new FileReader(gbkFile);
            BufferedReader gbkBfReader = new BufferedReader(gbkReader);
            String gbkLine;
            while ((gbkLine = gbkBfReader.readLine()) != null) {
                gbkData += gbkLine + "\n";
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Given file '" + gbkFile + "' was not found. "
                    + " Caused an error: " + ex);
        } catch (IOException ex) {
            System.out.println("Error encountered: " + ex);
        }
        return gbkData;
    }
}

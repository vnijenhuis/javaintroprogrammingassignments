/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice1_genbank_reader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author vnijenhuis
 */
public class GbkFeaturesFetcher {
    /**
     * Accession number.
     */
    private String accession;

    /**
     * Nucleotide sequence.
     */
    private String originSequence;

    /**
     * Length of the nucleotide sequence.
     */
    private String sequenceLength;

    /**
     * Definition of the organism.
     */
    private String definition;

    /**
     * Name of the organism/source.
     */
    private String organism;

    /**
     * List of features.
     */
    private ArrayList<String> features;

    /**
     * Gene counter.
     */
    private Integer geneCount = 0;

    /**
     * CDS counter.
     */
    private Integer cdsCount = 0;

    /**
     * Starting coordinates of a gene.
     */
    private Integer startCoordinates;

    /**
     * Ending coordinates of a gene.
     */
    private Integer endCoordinates;

    /**
     * Length of the reverse sequences.
     */
    private Integer complementLength = 0;

    /**
     * Length of the forward sequences.
     */
    private Integer forwardLength = 0;
    /**
     * Forward Reverse balance.
     */
    private int forwardReverseBalance;

    /**
     * CDS sequence.
     */
    private String cdsSequence;

    /**
     * Creates a HashMap.
     */
    private HashMap<String, String> iupacTable;

    /**
     * Fetches features for the option --summary.
     * @param gbkData data.
     * @return array list of generated values.
     */
    public final ArrayList fetchSummary(final String gbkData) {
        features = new ArrayList<>();
        // Search for organism
        Pattern orgPat = Pattern.compile("ORGANISM.*");
        Matcher orgMatch = orgPat.matcher(gbkData);
        if (orgMatch.find()) {
            fetchOrganism(orgMatch.group());
        }
        // Search for accession
        Pattern accPat = Pattern.compile("ACCESSION.*");
        Matcher accMatch = accPat.matcher(gbkData);
        if (accMatch.find()) {
            fetchAccession(accMatch.group());
        }
        // Search for definition
        Pattern defPat = Pattern.compile("DEFINITION.*");
        Matcher defMatch = defPat.matcher(gbkData);
        if (defMatch.find()) {
            fetchDefinition(defMatch.group());
        }
        // Search for gene name or locus tag
        Pattern genPat = Pattern.compile("gene.*[0-9]..[0-9]*");
        Matcher genMatch = genPat.matcher(gbkData);
        while (genMatch.find()) {
            this.geneCount += 1;
        }
        // Search for CDS
        Pattern cdsPat = Pattern.compile("CDS.*[0-9]..[0-9]*");
        Matcher cdsMatch = cdsPat.matcher(gbkData);
        while (cdsMatch.find()) {
            // Calculate forward and reverse lengths.
            String coordinates = cdsMatch.group().split("             ")[1];
            calculateFRlengths(coordinates);
            this.cdsCount += 1;
        }
        fetchSequenceData(gbkData);
        // Calculates FR balance with private integers.
        double balance = calculateFRbalance();
        // Add counters to features.
        features.add(Integer.toString(this.geneCount));
        features.add(Double.toString(balance));
        features.add(Integer.toString(this.cdsCount));
        return this.features;
    }

    /**
     * Gets the CDS features.
     * @param gbkData gene bank file data.
     * @param cdsProduct product specified by the user.
     * @return cds sequence.
     */
    public final String fetchCDSSequence(final String gbkData,
            final String cdsProduct) {
        int count = 0;
        Pattern cdsPat = Pattern.compile(cdsProduct);
        Matcher cdsMatch = cdsPat.matcher(gbkData);
        if (cdsMatch.find()) {
            int product = gbkData.indexOf("/product=\"" + cdsProduct + "\"");
            String subData = gbkData.substring(product);
            if (subData.contains("gene")) {
                int endPos = subData.indexOf("gene") - 1;
                int startPos = subData.indexOf("/translation=") + 14;
                String cdsData = subData.substring(startPos, endPos);
                String[] cdsDat = cdsData.split("");
                for (String seq: cdsDat) {
                    if (seq.matches("[A-Za-z]")) {
                        cdsSequence += seq;
                        count += 1;
                    }
                    if (count == 80) {
                        cdsSequence += "\n";
                        count = 0;
                    }
                }
            }
        }
        cdsSequence = cdsSequence.replaceFirst("null", "");
        return cdsSequence;
    }

    /**
     * Matches a given sequence and returns.
     * @param gbkData gene bank file data.
     * @param sequence sequence specified by user.
     */
    public final void fetchMatchingSiteFeatures(final String gbkData,
            final String sequence) {
                createIupacTable();
        String[] nucs = sequence.split("");
        String newSequence = "";
        for (String nuc: nucs) {
            if (iupacTable.containsKey(nuc)) {
                newSequence = sequence.replace(nuc, iupacTable.get(nuc));
            }
        }
        String lowerCaseSeq = newSequence.toLowerCase();
        Pattern seqPattern = Pattern.compile(lowerCaseSeq);
        Matcher seqMatch = seqPattern.matcher(gbkData);
//        while (seqMatch.find()) {
//            System.out.println("Matching sequence: " + seqMatch.group());
//        }
        throw new UnsupportedOperationException("Not supported yet.");
        // Allows the sequence to match on AAAGTTT and AAAATTT
        // use regex to match the search sequence on the whole genome sequence
        // get position and gene name
        // print results or add items to a list/map and return the list/map
    }
    /**
     * Fetches the definition from the gbk file.
     * @param definitionMatch matching string inside the gbk file.
     */
    public final void fetchDefinition(final String definitionMatch) {
        this.definition = definitionMatch.split("  ")[1];
        features.add(this.definition);
    }

    /**
     * Fetches the accession number from the gbk file.
     * @param accessionMatch matching string inside the gbk file.
     */
    public final void fetchAccession(final String accessionMatch) {
        this.accession = accessionMatch.split("  ")[1];
        features.add(this.accession);
    }
    /**
     * Fetches the organism/source name from the gbk file.
     * @param organismMatch matching string inside the gbk file.
     */
    public final void fetchOrganism(final String organismMatch) {
        this.organism = organismMatch.split("  ")[1];
        features.add(this.organism);
    }

    /**
     * Calculates Forward and Reverse strand lengths.
     * @param coordinates coordinates of the CDS.
     */
    public final void calculateFRlengths(final String coordinates) {
        determineCoordinates(coordinates);
        forwardLength += (endCoordinates - startCoordinates);
    }

    /**
     * calculates the FR balance.
     * @return FR balance as double.
     */
    public final double calculateFRbalance() {
        // forward divided by reverse (complement)
        this.forwardReverseBalance = (forwardLength / complementLength);
        double roundOff = (double) Math.round(forwardReverseBalance * 10) / 10;
        return roundOff;
    }

    /**
     * Fetches the sequence from the gbk file.
     * @param gbkData gbkData file.
     */
    public final void fetchSequenceData(final String gbkData) {
        int startPos = gbkData.indexOf("ORIGIN") + 6;
        int endPos = gbkData.indexOf("//");
        String seqData = gbkData.substring(startPos, endPos);
        this.originSequence = seqData.replaceAll("[0-9]", "");
        this.sequenceLength =  Integer.toString(this.originSequence.length());
        features.add(this.originSequence);
        features.add(this.sequenceLength);
    }

    /**
     * Determines coordinates.
     * @param coordinates coordinates.
     */
    private void determineCoordinates(final String coordinates) {
        if (coordinates.contains("complement")) {
            String coords = coordinates.split("complement\\(")[1];
            coords = coords.replace(")", "");
            startCoordinates = Integer.parseInt(
                    coords.split("\\..")[0]);
            endCoordinates = Integer.parseInt(coords.split("\\..")[1]);
            complementLength += (endCoordinates - startCoordinates);
        } else if (coordinates.contains("<")) {
            String coords = coordinates.split("<")[1];
            startCoordinates = Integer.parseInt(
                    coords.split("\\..")[0]);
            endCoordinates = Integer.parseInt(coords.split("\\..")[1]);
        } else {
            startCoordinates = Integer.parseInt(
                    coordinates.split("\\..")[0]);
            String coord = coordinates.split("\\..")[1];
            if (coord.contains("\n")) {
                endCoordinates = Integer.parseInt(coord.split("\n")[0]);
            } else {
                endCoordinates = Integer.parseInt(coord);
            }
        }
    }

    /**
     * Fetches the sequence of a gene for option fetch_gene.
     * @param gbkData gene bank file data.
     * @param gene gene specified by the user.
     * @return sequence.
     */
    public final String fetchGeneSequence(final String gbkData,
            final String gene) {
        int feature = gbkData.indexOf("FEATURES");
        int origin = gbkData.indexOf("ORIGIN") + 6;
        String featureData = gbkData.substring(feature, origin);
        // regex: (?=/gene="AXL2").*\n+[0-9] gets CDS coordinates.
        Pattern genPat = Pattern.compile("(gene.*\\n)(.*(?:/gene=\""
                + gene + "\"))");
        Matcher genMatch = genPat.matcher(featureData);
        if (genMatch.find()) {
            int count = 0;
            String coordinates = genMatch.group(1).split("            ")[1];
            determineCoordinates(coordinates);
            String sequence = gbkData.substring(origin);
            sequence = sequence.replaceAll("[0-9]", "");
            String[] splitSequence = sequence.split("");
                for (String seq: splitSequence) {
                    System.out.println(seq);
                    if (seq.matches("[GATCgatc]")) {
                        originSequence += seq;
                        count += 1;
                    }
                    if (count == 80) {
                        originSequence += "\n";
                        count = 0;
                    }
                }
        }
        originSequence = originSequence.replaceAll("null", "");
        return this.originSequence;
    }

    /**
     * Fetches the feature, type, start, stop and orientation
     * of the given coordinates.
     * @param gbkData gbk data.
     * @param coordinates coordinates such as 567..5616
     */
    public final void fetchSiteFeatures(final String gbkData,
            final String coordinates) {
        throw new UnsupportedOperationException("Not supported yet.");
        //Check if coordinates of gene(s) are present within the borders
        // of the given coordinates.
        // If present within borders get the gene name, gene type, starting
        // position, stop position and gene orientation.
        // split on .. of a coordinate to get start and stop positions.
        // if coordinates is a complement then orientation = R
        // if coordinates are normal or contain > or < then orientation = F
        // R = Reverse, F = forward
    }

    /**
     * Creates a IUPAC code table.
     */
    public final void createIupacTable() {
        iupacTable = new HashMap();
        iupacTable.put("R", "[AG]");
        iupacTable.put("Y", "[CT]");
        iupacTable.put("S", "[GC]");
        iupacTable.put("W", "[AT]");
        iupacTable.put("K", "[GT]");
        iupacTable.put("M", "[AC]");
        iupacTable.put("B", "[CGT]");
        iupacTable.put("D", "[AGT]");
        iupacTable.put("H", "[ACT]");
        iupacTable.put("V", "[ACG]");
        iupacTable.put("N", "[ACTG]");
        iupacTable.put(".", " ");
        iupacTable.put("-", " ");
    }
}

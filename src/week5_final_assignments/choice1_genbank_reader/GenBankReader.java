/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week5_final_assignments.choice1_genbank_reader;

import java.io.File;
import java.util.ArrayList;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class GenBankReader {
    /**
     * Creates options for the command line parser.
     */
    private final Options options;

    /**
     * Command line parser.
     */
    private final CommandLineParser gbkParser;

    /**
     * Fetch gene bank features.
     */
    private final GbkFeaturesFetcher fetcher;

        /**
     * The gene bank file.
     */

    private File gbkFile;

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        GenBankReader mainObject = new GenBankReader();
        mainObject.start(args);
//        String[] test1 = {"--infile", "data/example_genbank_file.gb",
//            "--summary"};
//        mainObject.start(test1);
//        String[] test4 = {"--infile", "data/Haloarcula_marismortui_genome.gb",
//            "--summary"};
//        mainObject.start(test4);
//        String[] test2 = {"--help"};
//        mainObject.start(test2);
//        String[] test3 = {"--infile", "data/example_genbank_file.gb",
//            "--fetch_gene", "AXL2"};
//        mainObject.start(test3);
//        String[] test4 = {"--infile", "data/example_genbank_file.gb",
//        "--fetch_cds", "Axl2p"};
//        mainObject.start(test4);
//        String[] test4 = {"--infile", "data/example_genbank_file.gb",
//        "--find_sites", "AAARTTT"};
//        mainObject.start(test4);
    }

    /**
     * private constructor.
     */
    private GenBankReader() {
        options = new Options();
        options.addOption("help", false, "Shows this help function");
        options.addOption("summary", false, "Textual summary of the file");
        options.addOption("infile", true, "gbk filepath");
        options.addOption("fetch_gene", true, "Returns nucleotide sequences "
                + "of the genes that match the gene name pattern,"
                + " in Fasta format");
        options.addOption("fetch_cds", true, "Returns the amino acid "
                + "sequences of the CDSs that match the product name pattern,"
                + " in Fasta format");
        options.addOption("fetch_features", true, "Returns all features with"
                + " name, type, start, stop and orientation between the "
                + "given coordinates");
        options.addOption("find_sites", true, "Lists the locations of all "
                + "the sites where the DNA pattern is found");
        gbkParser = new BasicParser();
        fetcher = new GbkFeaturesFetcher();
    }


    /**
     * Starts the application.
     * @param args the command line arguments passed from main()
     */
    private void start(final String[] args) {
        SingleGbkFileReader gbkReader = new SingleGbkFileReader();
        try {
            // Try to parse the commandline arguments.
            CommandLine line = gbkParser.parse(options, args);
            if (line.hasOption("help")) {
                getHelp();
            } else if (line.getOptionValue("infile").endsWith(".gb")
                    || line.getOptionValue("infile").endsWith(".gbk")) {
                this.gbkFile = new File(line.getOptionValue("infile"));
                String gbkData = gbkReader.gbkFileReader(gbkFile);
                // --infile data/example_genbank_file.gb --summary
                if (line.hasOption("summary")) {
                    getSummary(gbkData);
                }
                // --infile example_data/genbank_file.gb --fetch_gene AXL2
                if (line.hasOption("fetch_gene")) {
                    String gene = (String) line.getArgList().get(0);
                    getGeneFeatures(gbkData, gene);
                }
                // --infile data/example_genbank_file.gb --fetch_cds Rev7p
                if (line.hasOption("fetch_cds")) {
                    String cdsProduct = (String) line.getArgList().get(0);
                    getCDSSequence(gbkData, cdsProduct);
                }
                // --oinfile example_genbank_file.gb --fetch_features "50..100"
                if (line.hasOption("fetch_features")) {
                    String coordinates = (String) line.getArgList().get(0);
                    getFeatures(gbkData, coordinates);
                }
                // --infile data/example_genbank_file.gb --find_sites AAARTTT
                if (line.hasOption("find_sites")) {
                    String sequence =  args[3];
                    getSiteFeatures(gbkData, sequence);
                }
            }
        } catch (ParseException ex) {
            // Return exception if an error was encountered.
            System.out.println("Encountered a parser exception: " + ex);
        }
    }

    /**
     * Help function for the .gbk file reader.
     */
    public void getHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("ant", options);
    }

    /**
     * Summary of the gbk file.
     * @param gbkData data form the gene bank file.
     */
    public void getSummary(final String gbkData) {
        ArrayList summary = fetcher.fetchSummary(gbkData);
        System.out.println("file \t\t\t" + gbkFile);
        System.out.println("organism \t\t" + summary.get(0));
        System.out.println("accession \t\t" + summary.get(1));
        System.out.println("sequence length \t" + summary.get(4));
        System.out.println("number of genes \t" + summary.get(5));
        System.out.println("gene F/R balance \t" + summary.get(6));
        System.out.println("number of CDSs \t\t" + summary.get(7));
    }

    /**
     * Fetch features of the CDS.
     * @param gbkData data from the gene bank file.
     * @param cds product name of the CDS.
     */
    private void getCDSSequence(final String gbkData, final String cds) {
        String cdsSequence = fetcher.fetchCDSSequence(gbkData, cds);
        System.out.println(">cds " + cds + " sequence");
        System.out.println(cdsSequence);
    }

    /**
     *
     * @param gbkData data from the gene bank file.
     * @param gene gene name from command line input.
     */
    private void getGeneFeatures(final String gbkData, final String gene) {
        String sequence = fetcher.fetchGeneSequence(gbkData, gene);
        System.out.println(">gene " + gene + " sequence");
        System.out.println(sequence);
    }

    /**
     * Gets the features of given coordinates.
     * @param gbkData data from the gene bank file.
     * @param coordinates given coordinates from command line input.
     */
    private void getFeatures(final String gbkData, final String coordinates) {
        throw new UnsupportedOperationException("not supported yet");
    }

    /**
     * Gets position, sequence and gene from matching sequences.
     * @param gbkData data from the gene bank file.
     * @param sequence given sequence from command line input.
     */
    private void getSiteFeatures(final String gbkData, final String sequence) {
        fetcher.fetchMatchingSiteFeatures(gbkData, sequence);
    }
}
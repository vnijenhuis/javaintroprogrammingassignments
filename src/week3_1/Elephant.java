/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Elephant extends Animal {
    /**
     * Horse.
     */
    public Elephant() {
        this.name = "Elephant";
        this.maximumAge = 86;
        this.maximumSpeed = 40.0;
        this.movementType = "thunder";
    }

}

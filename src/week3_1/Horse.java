/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal {

    /**
     * Horse.
     */
    public Horse() {
        this.name = "Horse";
        this.maximumAge = 62;
        this.maximumSpeed = 88.0;
        this.movementType = "gallop";
    }
}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    /**
     * Executes the AnimerSimulator.
     * @param args contains command line arguments.
     */
    public static void main(final String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.start(args);
    }

    /**
     * Starts the Animal Simulator process.
     * @param args contains command line arguments.
     */
    private void start(final String[] args) {
        //start processing command line arguments
        Animal animal = new Animal();
        if (args.length > 0 && !Arrays.toString(args).contains("help")) {
            for (int i = 0; i < args.length; i++) {
                switch (args[i].toLowerCase()) {
                    case "elephant":
                        animal = new Elephant();
                        break;
                    case "horse":
                        animal = new Horse();
                        break;
                    case "mouse":
                        animal = new HouseMouse();
                        break;
                    case "tortoise":
                        animal = new Tortoise();
                        break;
                    default:
                        System.out.println("Error: animal species " + args[i]
                                + " is not known. run with single parameter "
                                + "\"help\" to get a listing of available "
                                + "species. Please give new values");
                        break;
                }
                Pattern pat = Pattern.compile("(^[EAOIU].+)");
                Matcher m = pat.matcher(args[i]);
                i++;
                int age = Integer.parseInt(args[i]);
                String name = animal.getName();
                String movement = animal.getMovementType();
                int maxAge = animal.getMaxAge();
                if (age > maxAge) {
                    System.out.println("Error: maximum age of "
                            + name + " is " + maxAge
                            + " years. Please give new values");
                } else if (age == 0) {
                    System.out.println("Error: age can't be zero.");
                } else {
                    double speed = animal.getSpeed(age);
                    // Checks if the prefix An or A has to be used.
                    if (m.find()) {
                        System.out.println("An " + name + " of age "
                                + args[i] + " moving in " + movement + " at "
                                + speed + " km/h");
                    } else {
                        System.out.println("A " + name + " of age "
                                + args[i] + " moving in " + movement + " at "
                                + speed + " km/h");
                    }
                }
            }
        } else {
            getSimulatorHelp();
        }
    }

    /**
     * returns all supported animals as List, alphabetically ordered.
     * @return supportedAnimals the supported animals
     */
    public final List<String> getSupportedAnimals() {
        List<String> supportedAnimals = new ArrayList<>();
        supportedAnimals.add("Elephant");
        supportedAnimals.add("Horse");
        supportedAnimals.add("Mouse");
        supportedAnimals.add("Tortoise");
        supportedAnimals.sort(null);
        return supportedAnimals;
    }

    /**
     * Help function for AnimalSimulator.
     */
    public final void getSimulatorHelp() {
        System.out.println("Usage: java AnimalSimulator "
                + "<Species age Species age ...>");
        System.out.println("Supported species (in alphabetical order):");
        for (int i = 0; i < getSupportedAnimals().size(); i++) {
            int j = i + 1;
            System.out.println(j + ": " + getSupportedAnimals().get(i));
        }
    }
}

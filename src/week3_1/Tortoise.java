/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Tortoise extends Animal {
    /**
     * Horse.
     */
    public Tortoise() {
        this.name = "Tortoise";
        this.maximumAge = 190;
        this.maximumSpeed = 0.3;
        this.movementType = "crawl";
    }
}

/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class HouseMouse extends Animal{
    /**
     * Mouse.
     */
    public HouseMouse() {
        this.name = "Mouse";
        this.maximumAge = 13;
        this.maximumSpeed = 21.0;
        this.movementType = "scurry";
    }
}

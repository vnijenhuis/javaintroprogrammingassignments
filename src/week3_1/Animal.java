/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import static java.lang.Math.ceil;
import static java.lang.Math.round;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    /**
     * Name of the animal.
     */
    String name;

    /**
     * Speed of the animal.
     */
    int maximumAge;
    double maximumSpeed;
    String movementType;

    /**
     * Returns the name of the animal.
     * @return name the species name
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Returns the maximum age of the animal.
     * @return the maximum age.
     */
    public final int getMaxAge() {
        return this.maximumAge;
    }

    /**
     * Returns the maximum speed of the animal.
     * @return the maximum speed.
     */
    public final double getMaxSpeed() {
        return this.maximumSpeed;
    }

    /**
     * Returns the movement type.
     * @return movementType the way the animal moves
     */
    public final String getMovementType() {
        return this.movementType;
    }

     /**
     * Calculates the speed of the animal.
     * @param age age of animal.
     * @return speed of animal.
     * speed = max_speed * (0.5 + (0.5 * ((max_age - age) / max_age)))
     */
    public final double getSpeed(final int currentAge) {
        float maxAge = (float) maximumAge;
        double speed = (double) (maximumSpeed * (0.5 + (0.5 * ((maxAge - currentAge) / maxAge))));
        double roundOff = (double) Math.round(speed * 10) / 10;
        return roundOff;
    }
}
